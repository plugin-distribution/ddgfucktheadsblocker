package nl.spiderdead.ddgfucktheadsblocker.Utils;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class Player {
    public static void sendMessage(String message) {
        Minecraft.getMinecraft().player.sendMessage(new TextComponentString(message).setStyle(new Style().setColor(TextFormatting.AQUA)));
    }
    public static void sendMessage(String message, TextFormatting color) {
        Minecraft.getMinecraft().player.sendMessage(new TextComponentString(message).setStyle(new Style().setColor(color)));
    }
}
