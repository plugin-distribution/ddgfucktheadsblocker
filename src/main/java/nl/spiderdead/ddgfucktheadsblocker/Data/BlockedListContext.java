package nl.spiderdead.ddgfucktheadsblocker.Data;

import nl.spiderdead.ddgfucktheadsblocker.Utils.ConfigHandler;

import java.util.ArrayList;

public class BlockedListContext {
    private ArrayList<String> blockedList = new ArrayList<>();

    public void init() {
        blockedList.add("crates");
        blockedList.add("crate");
        blockedList.add("[event]");
        blockedList.add("dusdavidgames.nl");
        blockedList.add("planetcraft");
        blockedList.add("oneblock");
        blockedList.add("minetopia");
        blockedList.add("teamcontrol");
        blockedList.add("teamspeak");
        blockedList.add("ddg");
        blockedList.add("teamcontrol");
        blockedList.add("boosters");
    }

    public ArrayList<String> getBlockedList() {
        return blockedList;
    }

    public void setBlockedList(ArrayList<String> blockedList) {
        this.blockedList = blockedList;
    }

    public void addWord(String string) {
        blockedList.add(string);
        ConfigHandler.writeConfig("blocklist", "blocked", String.join(";", blockedList));
    }

}