package nl.spiderdead.ddgfucktheadsblocker.Events;

import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.spiderdead.ddgfucktheadsblocker.Ddgfucktheadsblocker;
import nl.spiderdead.ddgfucktheadsblocker.Utils.Player;

import java.util.Arrays;

public class PlayerSendMessage {
    @SubscribeEvent
    public void onPlayerSendMessage(ClientChatEvent event) {
        if (event.getMessage().startsWith("ad#")) {
            event.setCanceled(true);

            String message = event.getMessage();
            String[] messageArray = message.split(" ");
            String[] args = Arrays.copyOfRange(messageArray, 1, messageArray.length);

            switch (messageArray[0].substring(3).toLowerCase()) {
                case "block":

                    if (args.length < 1) {
                        Player.sendMessage("[Adblocker] ad#block <block words...>");
                        return;
                    }

                    Ddgfucktheadsblocker.blockedListContext.addWord(String.join(" ", args));
                    Player.sendMessage(String.format("[Adblocker] %s toegevoegd aan de geblokkeerde woorden!", String.join(" ", args)));
                    break;
                case "config":
                    event.setCanceled(true);
                    Ddgfucktheadsblocker.INSTANCE.loadConfigVariables();
                    Player.sendMessage("[Adblocker] Config is herladen!");
                    break;
            }
        }
    }
}
