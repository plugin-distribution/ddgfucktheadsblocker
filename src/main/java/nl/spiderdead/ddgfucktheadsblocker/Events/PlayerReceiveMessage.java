package nl.spiderdead.ddgfucktheadsblocker.Events;

import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.spiderdead.ddgfucktheadsblocker.Ddgfucktheadsblocker;

public class PlayerReceiveMessage {
    @SubscribeEvent
    public void onPlayerReceiveMessage(ClientChatReceivedEvent event) {
        for (String blockedWord : Ddgfucktheadsblocker.blockedListContext.getBlockedList()) {
            if (event.getMessage().getUnformattedText().toLowerCase().contains(blockedWord)) {
                event.setCanceled(true);
            }
        }
    }
}
