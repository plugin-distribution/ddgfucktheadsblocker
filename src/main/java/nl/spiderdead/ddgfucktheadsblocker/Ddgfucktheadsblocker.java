package nl.spiderdead.ddgfucktheadsblocker;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import nl.spiderdead.ddgfucktheadsblocker.Data.BlockedListContext;
import nl.spiderdead.ddgfucktheadsblocker.Events.PlayerReceiveMessage;
import nl.spiderdead.ddgfucktheadsblocker.Events.PlayerSendMessage;
import nl.spiderdead.ddgfucktheadsblocker.Utils.ConfigHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

@Mod(
        modid = Ddgfucktheadsblocker.MOD_ID,
        name = Ddgfucktheadsblocker.MOD_NAME,
        version = Ddgfucktheadsblocker.VERSION
)
public class Ddgfucktheadsblocker {

    public static final String MOD_ID = "ddgfucktheadsblocker";
    public static final String MOD_NAME = "Ddgfucktheadsblocker";
    public static final String VERSION = "4.20";

    public static BlockedListContext blockedListContext;

    /**
     * This is the instance of your mod as created by Forge. It will never be null.
     */
    @Mod.Instance(MOD_ID)
    public static Ddgfucktheadsblocker INSTANCE;

    /**
     * This is the first initialization event. Register tile entities here.
     * The registry events below will have fired prior to entry to this method.
     */
    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {

    }

    /**
     * This is the second initialization event. Register custom recipes
     */
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        blockedListContext = new BlockedListContext();
        blockedListContext.init();
        initConfig();
        loadConfigVariables();
    }

    /**
     * This is the final initialization event. Register actions from other mods here
     */
    @Mod.EventHandler
    public void postinit(FMLPostInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new PlayerReceiveMessage());
        MinecraftForge.EVENT_BUS.register(new PlayerSendMessage());
    }

    private void initConfig() {
        ConfigHandler.init();

        if (ConfigHandler.getString("blocklist", "blocked") == null)
            ConfigHandler.writeConfig("blocklist", "blocked", String.join(";", blockedListContext.getBlockedList()));
    }

    public void loadConfigVariables() {
        blockedListContext.setBlockedList(
                new ArrayList<>(
                        Arrays.asList(
                                Objects.requireNonNull(
                                        ConfigHandler.getString("blocklist", "blocked")
                                ).split(";")
                        )
                )
        );
    }
}
